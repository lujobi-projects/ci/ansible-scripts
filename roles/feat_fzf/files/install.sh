#!/bin/bash

git clone --depth 1 https://github.com/junegunn/fzf.git $1/.fzf
$1/.fzf/install
