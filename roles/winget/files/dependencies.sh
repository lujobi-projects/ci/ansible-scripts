#!/bin/bash

winget.exe install -e --id Mozilla.Firefox
winget.exe install -e --id AgileBits.1Password
winget.exe install -e --id Docker.DockerDesktop
winget.exe install -e --id Telegram.TelegramDesktop
winget.exe install -e --id Microsoft.PowerToys
winget.exe install -e --id Valve.Steam
winget.exe install -e --id Discord.Discord
winget.exe install -e --id Spotify.Spotify
winget.exe install -e --id Obsidian.Obsidian
winget.exe install -e --id Git.Git
winget.exe install -e --id Google.GoogleDrive
winget.exe install -e --id Microsoft.PowerToys
winget.exe install -e --id Microsoft.VisualStudioCode
winget.exe install -e --id Mobatek.MobaXterm
winget.exe install -e --id 7zip.7zip
winget.exe install -e --id jstarks.npiperelay
winget.exe install -e --id Ultimaker.Cura
winget.exe install -e --id Parsec.Parsec
