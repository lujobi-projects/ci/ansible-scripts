lint:
	ansible-lint 01-wsl-setup-prerequisites.yml
	ansible-lint 02-wsl-setup-features.yml
	ansible-lint wsl-update.yml

lint-fix:
	ansible-lint 01-wsl-setup-prerequisites.yml --fix
	ansible-lint 02-wsl-setup-features.yml --fix
	ansible-lint wsl-update.yml --fix

update:
	ansible-playbook -K wsl-update.yml

setup:
	ansible-playbook -K 01-wsl-setup-prerequisites.yml
	ansible-playbook -K 02-wsl-setup-features.yml

update-wsl: update
	wsl.exe --update
	