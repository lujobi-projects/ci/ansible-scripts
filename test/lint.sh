#!/bin/bash

set -eu
set -o pipefail

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
CONTAINER_NAME="testing_container"

docker build -t $CONTAINER_NAME -f $SCRIPT_DIR/Dockerfile $SCRIPT_DIR/..
docker run -it $CONTAINER_NAME /home/root/test/lint_internal.sh
