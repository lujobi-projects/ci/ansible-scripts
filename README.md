# Ansible setup

```bash 
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

testing 
```bash
PATH=/root/.local/bin:$PATH /bin/bash -c "make setup"
```
